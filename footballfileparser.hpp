#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

#ifndef FOOTBALLFILEPARSER_HPP_
#define FOOTBALLFILEPARSER_HPP_

class FootballFileParser
{
    private:
        string name;
      	ifstream file;
	     string line;
    	vector<string> data;

    ifstream superbowl ;
    public:
        FootballFileParser(const string &filepath);
        bool read();  // Returns false if parsing fails
        void printStats() const; // prints out the stats of game
        vector<string> returnVector();
};