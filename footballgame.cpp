#include "footballgame.hpp"

FootballGame::FootballGame(const string &homeName, const string &visitorsName)
{
    // Constructor for FootballGame
    // Initialize the homeName and visitorsName member variables to the given arguments
    this->homeName = homeName;
    this->visitorsName = visitorsName;
    // Create new FootballTeam objects for the home and visitors teams
    this->home = new FootballTeam(this->homeName);
    this->visitors = new FootballTeam(this->visitorsName);
    // Initialize the scores and MVP scores to 0
    this->mvp_home_score = 0;
    this->mvp_visitors_score = 0;
    this->home_score = 0;
    this->visitors_score = 0;
}

FootballGame::~FootballGame()
{
    // Destructor for FootballGame
    // Deallocate memory for the home and visitors FootballTeam objects
    this->home->~FootballTeam();
    this->visitors->~FootballTeam();
}

void FootballGame::addPlayer(Side side, const string &name)
{
    // Method to add a new player to a team
    if(side==HOME)
    {
        // If the player is on the home team, call the addPlayer method for the home team object
        this->home->addPlayer(name);
    }
    else
    {
        // If the player is on the visitors team, call the addPlayer method for the visitors team object
        this->visitors->addPlayer(name);
    }
}


void FootballGame::recordScore(Side side, const string& playerName, int points)
{
    if(side==HOME)
    {
        this->home->getPlayer(playerName)->recordScore(points);
        this->home_score += points;
        if(points > this->mvp_home_score)
        {
            this->mvp_home_score = points;
            this->mvp_home = playerName;
        }
    }
    else
    {
        this->visitors->getPlayer(playerName)->recordScore(points);
        this->visitors_score += points;
        if(points > this->mvp_visitors_score)
        {
            this->mvp_visitors_score = points;
            this->mvp_visitors = playerName;
        }
    }
}

FootballGameStats FootballGame::getStats() const
{
    FootballTeamStats homeStats, visitorsStats;
    FootballGameStats gameStats;

    homeStats.score = this->home_score;
    homeStats.mvpName = this->mvp_home;
    homeStats.mvpScore = this->mvp_home_score; //assigns home team states


    visitorsStats.score = this->visitors_score;
    visitorsStats.mvpName = this->mvp_visitors;
    visitorsStats.mvpScore = this->mvp_visitors_score; //assign visitor stats

    if(this->home_score >= this->visitors_score) //determine winner of game
    {
        gameStats.winner = Side::HOME;
    }
    else
    {
        gameStats.winner = Side::VISITORS;
    }

    gameStats.homeStats = homeStats;
    gameStats.visitorsStats = visitorsStats;
    return gameStats;
}
