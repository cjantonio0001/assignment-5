#include <iostream>
#include <string>

using namespace std;

enum Side { HOME, VISITORS };

const int MAX_PLAYERS = 100;

class FootballPlayer {
private:
    // TODO
public:
    FootballPlayer(const string &name);
    const string &getName() const;
    int getPointsScored() const;
    void recordScore(int points);
};

// TODO: FootballPlayer methods


class FootballTeam {
private:
    // TODO
public:
    FootballTeam(const string &name);
    ~FootballTeam();
    void addPlayer(const string &name);
    FootballPlayer *getPlayer(const string &name) const;
};

// TODO: FootballTeam methods


struct FootballTeamStats {
    int score;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

class FootballGame {
private:
    // TODO
public:
    FootballGame(const string &homeName, const string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const string &name);
    void recordScore(Side side, const string& playerName, int points);
    FootballGameStats getStats() const;
};

// TODO: FootballGame methods


class FootballFileParser {
private:
    // TODO
public:
    FootballFileParser(const string &filepath);
    bool read();  // Returns false if parsing fails
    void printStats() const;
};

// TODO: FootballFileParser methods


int main(int argc, char **argv)
{
    // TODO
}
