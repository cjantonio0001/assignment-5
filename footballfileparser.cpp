#include "footballfileparser.hpp"

FootballFileParser::FootballFileParser(const string &filepath)
{
  name = filepath ; 
}

bool FootballFileParser::read()
{
    file.open(name);
	if(!file){
		cout << "invalid file";
		return false;
	}
	while(getline(file, line)){
		data.push_back(line);
	}
	return true;

}

// This function prints the stats for the game parsed by the FootballFileParser
/*void FootballFileParser::printStats() const
{

string winner = "";
FootballGameStats stats = this->s.data->getStats();
    if(stats.winner==Side::HOME) { winner="Home"; }
    else { winner="Visitors"; }

    cout<<"Home: "<<stats.homeStats.score<<endl;
    cout<<"Visitors: "<<stats.visitorsStats.score<<endl;
    cout<<"Winner: "<<winner<<endl;
    cout<<"Highest scorers:\n";
    cout<<" Home:"<<stats.homeStats.mvpName<<" ("<<stats.homeStats.mvpScore<<" points)\n";
    cout<<" Visitors:"<<stats.visitorsStats.mvpName<<" ("<<stats.visitorsStats.mvpScore<<" points)";
}
void FootballFileParser::printStats() const{
	for(string s : data){
		cout << s << endl;
	}
}
vector<string> FootballFileParser::returnVector(){
	return data;
}