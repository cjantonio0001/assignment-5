#include <iostream>
#include "footballplayer.hpp"

#ifndef FOOTBALLTEAM_HPP_
#define FOOTBALLTEAM_HPP_

class FootballTeam
{
    private:
        string name; // team name
        int numPlayers; // number of players on team
        FootballPlayer** players; //players on team
    public:
        FootballTeam(const string &name);
        ~FootballTeam();
        void addPlayer(const string &name);
        FootballPlayer *getPlayer(const string &name) const;
};