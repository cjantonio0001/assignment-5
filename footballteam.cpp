#include "footballteam.hpp"

FootballTeam::~FootballTeam()
{
    // Loop through each player in the players array to delete
    for (int player_index = 0; player_index < this->numPlayers; player_index++)
    {
        delete this->players[player_index];
    }
    
    delete[] this->players; //delete array
}

void FootballTeam::addPlayer(const string &name)
{
    //creates player object w name
    if(numPlayers < MAX_PLAYERS)
    {
        this->players[numPlayers++] = new FootballPlayer(name);
    }
}

FootballPlayer* FootballTeam::getPlayer(const string& name) const
{
    for (int player_index = 0; player_index < this->numPlayers; player_index++)
    {
       
        if (this->players[player_index]->getName() == name)
        {
            return this->players[player_index];
        }//matches player w name from file and returns pointer
    }
    // If the player was not found, return a null pointer
    return nullptr;
}
