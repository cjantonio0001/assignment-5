#include "footballteam.hpp"

#ifndef FOOTBALLGAME_HPP_
#define FOOTBALLGAME_HPP_

struct FootballTeamStats
{
    int score;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats
{
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

class FootballGame
{
    private:
        string homeName;
        string visitorsName;
        FootballTeam* home;
        FootballTeam* visitors;
        int mvp_home_score, mvp_visitors_score;
        int home_score, visitors_score;
        string mvp_home, mvp_visitors;
    public:
        FootballGame(const string &homeName, const string &visitorsName);
        ~FootballGame();
        void addPlayer(Side side, const string &name);
        void recordScore(Side side, const string& playerName, int points);
        FootballGameStats getStats() const;
};