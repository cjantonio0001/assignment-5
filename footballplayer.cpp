#include "footballplayer.hpp"
#include <string>


FootballPlayer::FootballPlayer(const string &name)
{
this->name = name; // initialize the name of the player
this->points = 0; // initialize points player scores
}

// Method to get the name of the player
const string& FootballPlayer::getName() const
{
return this->name; // return player name
}

// Method to get the points scored by the player
int FootballPlayer::getPointsScored() const
{
return this->points; // return the points scored by the player
}

// Method to record the points scored by the player
void FootballPlayer::recordScore(int points)
{
this->points = points; // set the points scored by the player
}

#include 
FootballPlayer::FootballPlayer(const string &name)
{
this->name = name; // initialize the name of the player
this->points = 0; // initialize points player scores
}

// Method to get the name of the player
const string& FootballPlayer::getName() const
{
return this->name; // return player name
}

// Method to get the points scored by the player
int FootballPlayer::getPointsScored() const
{
return this->points; // return the points scored by the player
}

// Method to record the points scored by the player
void FootballPlayer::recordScore(int points)
{
this->points = points; // set the points scored by the player
}

