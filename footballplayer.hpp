#include <iostream>
using namespace std;

#ifndef FOOTBALLPLAYER_HPP_
#define FOOTBALLPLAYER_HPP_

class FootballPlayer
{
    private:
        string name; // name of player
        int points; //points player scores
    public:
        FootballPlayer(const string &name);
        const string &getName() const;
        int getPointsScored() const;
        void recordScore(int points);
};