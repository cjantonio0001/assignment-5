#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;
 enum Side { HOME, VISITORS };

const int MAX_PLAYERS = 100;

class FootballPlayer
{
    private:
        string name; // name of player
        int points; //points player scores
    public:
        FootballPlayer(const string &name);
        const string &getName() const;
        int getPointsScored() const;
        void recordScore(int points);
};

// TODO: FootballPlayer methods
// Football player constructor
FootballPlayer::FootballPlayer(const string &name)
{
this->name = name; // initialize the name of the player
this->points = 0; // initialize points player scores
}

// Method to get the name of the player
const string& FootballPlayer::getName() const
{
return this->name; // return player name
}

// Method to get the points scored by the player
int FootballPlayer::getPointsScored() const
{
return this->points; // return the points scored by the player
}

// Method to record the points scored by the player
void FootballPlayer::recordScore(int points)
{
this->points = points; // set the points scored by the player
}


class FootballTeam
{
    private:
        string name; // team name
        int numPlayers; // number of players on team
        FootballPlayer** players; //players on team
    public:
        FootballTeam(const string &name);
        ~FootballTeam();
        void addPlayer(const string &name);
        FootballPlayer *getPlayer(const string &name) const;
};

// TODO: FootballTeam methods


FootballTeam::~FootballTeam()
{
    // Loop through each player in the players array to delete
    for (int player_index = 0; player_index < this->numPlayers; player_index++)
    {
        delete this->players[player_index];
    }
    
    delete[] this->players; //delete array
}

void FootballTeam::addPlayer(const string &name)
{
    //creates player object w name
    if(numPlayers < MAX_PLAYERS)
    {
        this->players[numPlayers++] = new FootballPlayer(name);
    }
}

FootballPlayer* FootballTeam::getPlayer(const string& name) const
{
    for (int player_index = 0; player_index < this->numPlayers; player_index++)
    {
       
        if (this->players[player_index]->getName() == name)
        {
            return this->players[player_index];
        }//matches player w name from file and returns pointer
    }
    // If the player was not found, return a null pointer
    return nullptr;
}


struct FootballTeamStats
{
    int score;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats
{
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

class FootballGame
{
    private:
        string homeName;
        string visitorsName;
        FootballTeam* home;
        FootballTeam* visitors;
        int mvp_home_score, mvp_visitors_score;
        int home_score, visitors_score;
        string mvp_home, mvp_visitors;
    public:
        FootballGame(const string &homeName, const string &visitorsName);
        ~FootballGame();
        void addPlayer(Side side, const string &name);
        void recordScore(Side side, const string& playerName, int points);
        FootballGameStats getStats() const;
};

// TODO: FootballGame methods
// FootballGame methods
FootballGame::FootballGame(const string &homeName, const string &visitorsName)
{
    // Constructor for FootballGame
    // Initialize the homeName and visitorsName member variables to the given arguments
    this->homeName = homeName;
    this->visitorsName = visitorsName;
    // Create new FootballTeam objects for the home and visitors teams
    this->home = new FootballTeam(this->homeName);
    this->visitors = new FootballTeam(this->visitorsName);
    // Initialize the scores and MVP scores to 0
    this->mvp_home_score = 0;
    this->mvp_visitors_score = 0;
    this->home_score = 0;
    this->visitors_score = 0;
}

FootballGame::~FootballGame()
{
    // Destructor for FootballGame
    // Deallocate memory for the home and visitors FootballTeam objects
    this->home->~FootballTeam();
    this->visitors->~FootballTeam();
}

void FootballGame::addPlayer(Side side, const string &name)
{
    // Method to add a new player to a team
    if(side==HOME)
    {
        // If the player is on the home team, call the addPlayer method for the home team object
        this->home->addPlayer(name);
    }
    else
    {
        // If the player is on the visitors team, call the addPlayer method for the visitors team object
        this->visitors->addPlayer(name);
    }
}


void FootballGame::recordScore(Side side, const string& playerName, int points)
{
    if(side==HOME)
    {
        this->home->getPlayer(playerName)->recordScore(points);
        this->home_score += points;
        if(points > this->mvp_home_score)
        {
            this->mvp_home_score = points;
            this->mvp_home = playerName;
        }
    }
    else
    {
        this->visitors->getPlayer(playerName)->recordScore(points);
        this->visitors_score += points;
        if(points > this->mvp_visitors_score)
        {
            this->mvp_visitors_score = points;
            this->mvp_visitors = playerName;
        }
    }
}

FootballGameStats FootballGame::getStats() const
{
    FootballTeamStats homeStats, visitorsStats;
    FootballGameStats gameStats;

    homeStats.score = this->home_score;
    homeStats.mvpName = this->mvp_home;
    homeStats.mvpScore = this->mvp_home_score; //assigns home team states


    visitorsStats.score = this->visitors_score;
    visitorsStats.mvpName = this->mvp_visitors;
    visitorsStats.mvpScore = this->mvp_visitors_score; //assign visitor stats

    if(this->home_score >= this->visitors_score) //determine winner of game
    {
        gameStats.winner = Side::HOME;
    }
    else
    {
        gameStats.winner = Side::VISITORS;
    }

    gameStats.homeStats = homeStats;
    gameStats.visitorsStats = visitorsStats;
    return gameStats;
}


class FootballFileParser
{
    private:
        string file;
    ifstream superbowl ;
    public:
        FootballFileParser(const string &filepath);
        bool read();  // Returns false if parsing fails
        void printStats() const; // prints out the stats of game
};

// TODO: FootballFileParser methods
FootballFileParser::FootballFileParser(const string &file)
{
    this->file = file;
}

bool FootballFileParser::read()
{
    //i didnt know what ot put here
}

// This function prints the stats for the game parsed by the FootballFileParser
void FootballFileParser::printStats() const
{

string winner = "";
FootballGameStats stats = this->game->getStats();
    if(stats.winner==Side::HOME) { winner="Home"; }
    else { winner="Visitors"; }

    cout<<"Home: "<<stats.homeStats.score<<endl;
    cout<<"Visitors: "<<stats.visitorsStats.score<<endl;
    cout<<"Winner: "<<winner<<endl;
    cout<<"Highest scorers:\n";
    cout<<" Home:"<<stats.homeStats.mvpName<<" ("<<stats.homeStats.mvpScore<<" points)\n";
    cout<<" Visitors:"<<stats.visitorsStats.mvpName<<" ("<<stats.visitorsStats.mvpScore<<" points)";
}

int main(int argc, char **argv)
{
    // TODO
    // Check if the command line arguments have a file path specified.
    if (argc == 2)
    {
        string file = argv[1]; // use superbowl.csv
        
        FootballFileParser parser(file);
        
        // Call the read method on the parser object to parse the file.
        parser.read();
        
        // Call the printStats method on the parser object to display the parsed statistics.
        parser.printStats();
    }
    else
        cout << "enter 2 command line arguments" << endl;
}
